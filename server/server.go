package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"

	"gitlab.com/alfianyuandika/test-sushi-tei/menu"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
)

func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

type server struct {
	menu.UnimplementedMenuServiceServer
}

// type category_item struct {
// 	Id                  primitive.ObjectID `bson:"_id, omitempty"`
// 	Category            string             `bson:"category"`
// 	CategoryDescription string             `bson:"categoryDescription"`
// 	Menu                *menu_item         `bson:"price"`
// }

type menu_item struct {
	Id              primitive.ObjectID `bson:"_id,omitempty"`
	MenuName        string             `bson:"menuName"`
	MenuDescription string             `bson:"menuDescription"`
	Price           int32              `bson:"price"`
}

//var Category_Item []category_item

func pushUserToDb(ctx context.Context, item menu_item) primitive.ObjectID {
	res, err := collection.InsertOne(ctx, item)
	handleError(err)

	return res.InsertedID.(primitive.ObjectID)
}

func (*server) MenuRequest(ctx context.Context, req *menu.Menu) (*menu.MenuResponse, error) {
	fmt.Println("Menu() called")
	// category := req.GetCategory()
	// categoryDescription := req.GetCategoryDescription()
	// menu :=
	// {
	menuName := req.GetMenuName()
	menuDescription := req.GetMenuDescription()
	price := req.GetPrice()
	// }
	newMenuItem := menu_item{
		// Category:            category,
		// CategoryDescription: categoryDescription,
		// Menu: *&menu_item{
		//menu{
		MenuName:        menuName,
		MenuDescription: menuDescription,
		Price:           price,
		//},
		// },
	}
	docid := pushUserToDb(ctx, newMenuItem)
	result := fmt.Sprintf("Menu name: %v\nMenu description: %v\nPrice: %v\nNewly created docid is %v", menuName, menuDescription, price, docid)

	menuResponse := menu.MenuResponse{

		Result: result,
	}

	return &menuResponse, nil
}

var collection *mongo.Collection

func main() {
	opts := []grpc.ServerOption{}
	s := grpc.NewServer(opts...)

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	handleError(err)
	menu.RegisterMenuServiceServer(s, &server{})

	go func() {
		if err := s.Serve(lis); err != nil {
			handleError(err)
		}
	}()

	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb+srv://AlfianOrdent:AlfianOrdent@ordentsushitei.9wofq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"))
	handleError(err)
	fmt.Println("MongoDB Connected")

	err = client.Connect(context.TODO())
	handleError(err)

	collection = client.Database("SushiTei").Collection("MenuShushiTei")

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	<-ch

	fmt.Println("Closing Mongo Connection")
	if err := client.Disconnect(context.TODO()); err != nil {
		handleError(err)
	}

	s.Stop()
}
