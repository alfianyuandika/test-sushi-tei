package main

import (
	"context"
	"fmt"
	"log"

	// "github.com/gin-gonic/gin"
	"gitlab.com/alfianyuandika/test-sushi-tei/menu"
	"google.golang.org/grpc"
)

func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func MenuRequest(c menu.MenuServiceClient) {
	menuRequest := menu.Menu{
		// Category:            "Gunkan",
		// CategoryDescription: "Gunkan adalah makanan khas korea",
		// Menu: &menu.Menu{
		MenuName:        "Chuka Kurage",
		MenuDescription: "Jellyfish",
		Price:           21000,
		// },
	}
	c.MenuRequest(context.Background(), &menuRequest)
}

func main() {
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	handleError(err)

	// g := gin.Default

	fmt.Println("Client Started")
	defer conn.Close()

	c := menu.NewMenuServiceClient(conn)

	MenuRequest(c)
}
